﻿
namespace nivel_de_tanque_25
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.paneltanque = new System.Windows.Forms.Panel();
            this.panelagua = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.pvxtanque = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pbxparo = new System.Windows.Forms.PictureBox();
            this.pbxvaciar = new System.Windows.Forms.PictureBox();
            this.pbxllenar = new System.Windows.Forms.PictureBox();
            this.btnparar = new System.Windows.Forms.Button();
            this.btnvaciar = new System.Windows.Forms.Button();
            this.btnllenar = new System.Windows.Forms.Button();
            this.tmrllenar = new System.Windows.Forms.Timer(this.components);
            this.tmrvaciar = new System.Windows.Forms.Timer(this.components);
            this.tmrvariable = new System.Windows.Forms.Timer(this.components);
            this.paneltanque.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pvxtanque)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxparo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxvaciar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxllenar)).BeginInit();
            this.SuspendLayout();
            // 
            // paneltanque
            // 
            this.paneltanque.BackColor = System.Drawing.Color.Blue;
            this.paneltanque.Controls.Add(this.panelagua);
            this.paneltanque.Location = new System.Drawing.Point(0, 0);
            this.paneltanque.Name = "paneltanque";
            this.paneltanque.Size = new System.Drawing.Size(32, 156);
            this.paneltanque.TabIndex = 0;
            // 
            // panelagua
            // 
            this.panelagua.BackColor = System.Drawing.Color.White;
            this.panelagua.Location = new System.Drawing.Point(0, 0);
            this.panelagua.Name = "panelagua";
            this.panelagua.Size = new System.Drawing.Size(32, 159);
            this.panelagua.TabIndex = 2;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.panel2.Controls.Add(this.paneltanque);
            this.panel2.Location = new System.Drawing.Point(386, 132);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(32, 156);
            this.panel2.TabIndex = 1;
            // 
            // pvxtanque
            // 
            this.pvxtanque.Image = global::nivel_de_tanque_25.Properties.Resources.Recurso_1tank2;
            this.pvxtanque.Location = new System.Drawing.Point(354, 45);
            this.pvxtanque.Name = "pvxtanque";
            this.pvxtanque.Size = new System.Drawing.Size(349, 277);
            this.pvxtanque.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pvxtanque.TabIndex = 3;
            this.pvxtanque.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.panel1.Controls.Add(this.pbxparo);
            this.panel1.Controls.Add(this.pbxvaciar);
            this.panel1.Controls.Add(this.pbxllenar);
            this.panel1.Controls.Add(this.btnparar);
            this.panel1.Controls.Add(this.btnvaciar);
            this.panel1.Controls.Add(this.btnllenar);
            this.panel1.Location = new System.Drawing.Point(66, 102);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(199, 202);
            this.panel1.TabIndex = 4;
            // 
            // pbxparo
            // 
            this.pbxparo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.pbxparo.Location = new System.Drawing.Point(128, 139);
            this.pbxparo.Name = "pbxparo";
            this.pbxparo.Size = new System.Drawing.Size(33, 31);
            this.pbxparo.TabIndex = 10;
            this.pbxparo.TabStop = false;
            // 
            // pbxvaciar
            // 
            this.pbxvaciar.BackColor = System.Drawing.Color.DarkGreen;
            this.pbxvaciar.Location = new System.Drawing.Point(128, 83);
            this.pbxvaciar.Name = "pbxvaciar";
            this.pbxvaciar.Size = new System.Drawing.Size(33, 32);
            this.pbxvaciar.TabIndex = 9;
            this.pbxvaciar.TabStop = false;
            // 
            // pbxllenar
            // 
            this.pbxllenar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.pbxllenar.Location = new System.Drawing.Point(128, 31);
            this.pbxllenar.Name = "pbxllenar";
            this.pbxllenar.Size = new System.Drawing.Size(33, 34);
            this.pbxllenar.TabIndex = 8;
            this.pbxllenar.TabStop = false;
            // 
            // btnparar
            // 
            this.btnparar.Location = new System.Drawing.Point(19, 139);
            this.btnparar.Name = "btnparar";
            this.btnparar.Size = new System.Drawing.Size(86, 31);
            this.btnparar.TabIndex = 7;
            this.btnparar.Text = "parar";
            this.btnparar.UseVisualStyleBackColor = true;
            this.btnparar.Click += new System.EventHandler(this.btnparar_Click);
            // 
            // btnvaciar
            // 
            this.btnvaciar.Location = new System.Drawing.Point(19, 83);
            this.btnvaciar.Name = "btnvaciar";
            this.btnvaciar.Size = new System.Drawing.Size(86, 30);
            this.btnvaciar.TabIndex = 6;
            this.btnvaciar.Text = "vaciar";
            this.btnvaciar.UseVisualStyleBackColor = true;
            this.btnvaciar.Click += new System.EventHandler(this.btnvaciar_Click);
            // 
            // btnllenar
            // 
            this.btnllenar.Location = new System.Drawing.Point(19, 30);
            this.btnllenar.Name = "btnllenar";
            this.btnllenar.Size = new System.Drawing.Size(86, 35);
            this.btnllenar.TabIndex = 5;
            this.btnllenar.Text = "llenar";
            this.btnllenar.UseVisualStyleBackColor = true;
            this.btnllenar.Click += new System.EventHandler(this.btnllenar_Click);
            // 
            // tmrllenar
            // 
            this.tmrllenar.Interval = 50;
            this.tmrllenar.Tick += new System.EventHandler(this.tmrllenar_Tick);
            // 
            // tmrvaciar
            // 
            this.tmrvaciar.Interval = 50;
            this.tmrvaciar.Tick += new System.EventHandler(this.tmrvaciar_Tick);
            // 
            // tmrvariable
            // 
            this.tmrvariable.Enabled = true;
            this.tmrvariable.Interval = 500;
            this.tmrvariable.Tick += new System.EventHandler(this.tmrvariable_Tick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(727, 373);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.pvxtanque);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "nivel de tanque ";
            this.paneltanque.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pvxtanque)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbxparo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxvaciar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxllenar)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel paneltanque;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panelagua;
        private System.Windows.Forms.PictureBox pvxtanque;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pbxvaciar;
        private System.Windows.Forms.PictureBox pbxllenar;
        private System.Windows.Forms.Button btnparar;
        private System.Windows.Forms.Button btnvaciar;
        private System.Windows.Forms.Button btnllenar;
        private System.Windows.Forms.Timer tmrllenar;
        private System.Windows.Forms.Timer tmrvaciar;
        private System.Windows.Forms.Timer tmrvariable;
        private System.Windows.Forms.PictureBox pbxparo;
    }
}

