﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace nivel_de_tanque_25
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnllenar_Click(object sender, EventArgs e)
        {
            tmrvaciar.Enabled = true;
            tmrllenar.Enabled = false;
        }

        private void btnvaciar_Click(object sender, EventArgs e)
        {
            tmrllenar.Enabled = true;
            tmrvaciar.Enabled = false;
        }

        private void tmrllenar_Tick(object sender, EventArgs e)
        {
            if(panelagua.Height < paneltanque.Height)
            {
                panelagua.Height += 2;
            }
            else
            {
                tmrllenar.Enabled = false;
            }

        }
        private void tmrvaciar_Tick(object sender, EventArgs e)
        {
            if(panelagua.Height > 0)
            {
                panelagua.Height -= 2;
            }
            else
            {
                tmrvaciar.Enabled = false;
            }

        }
        private void tmrvariable_Tick(object sender, EventArgs e)
        {
            if(tmrllenar.Enabled == true)
            {
                pbxllenar.BackColor = Color.Lime;
            }
            else
            {
                pbxllenar.BackColor = Color.FromArgb(0, 32, 0);
            }  

            if (tmrvaciar.Enabled == true)
            {
                pbxvaciar.BackColor = Color.Lime;
            }
            else
            {
                pbxvaciar.BackColor = Color.FromArgb(0, 32, 0);
            }

            if(tmrllenar.Enabled == false && tmrvaciar.Enabled == false)
            {
                pbxparo.BackColor = Color.Red;
            }
            else
            {
                pbxvaciar.BackColor = Color.FromArgb(32, 0, 0);
            }
        }

        private void btnparar_Click(object sender, EventArgs e)
        {
            tmrvaciar.Enabled = false;
            tmrllenar.Enabled = false;
        }

    }
}











